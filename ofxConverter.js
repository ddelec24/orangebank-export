/**
 * 10/10/2019
 * Damien D.
 * https://gitlab.com/orangebank-csv
 */

'use strict';

class ofxConverter {

	constructor() {
		this._br = "\n";
		this._tab = "\t";
		this._currency = "EUR";
		this._accttype = "CREDITCARD"; // ACCTTYPE accepte 'CHECKING', 'SAVINGS', 'CREDITCARD'
	}

	generateOFX(bankName = "Orange Bank", accountNumber = "00000000", balance = "0", transactions, currentDate)
	{

		let dataDate = this._form_ymdHis(new Date()); // formate la date courante au format ymdHis

		if(transactions) {
			let generatedOFX = this.getOFX2Header(bankName, accountNumber, dataDate);
			generatedOFX += this.getOFX2Transactions(transactions, dataDate);
			generatedOFX += this.getOFX2Footer(balance, dataDate);

			const MIME_OFX = 'data:application/octet-stream;charset=utf-8,';
			let filename = bankName + " - " + accountNumber + " - " + currentDate + ".ofx";
    		this._downloadFile(MIME_OFX + encodeURIComponent(generatedOFX), filename);
		} else {
			console.log("Erreur d'analyse des transactions.");
			//alert("Impossible de traiter la demande actuellement, merci de rafraîchir la page et retenter");
		}
	}

	getOFX2Header(bankName, accountNumber, dataDate)
    {
    	let ofx = "<?xml version=\"1.0\" encoding=\"utf-8\"? >";
		ofx += this._br;
		ofx += "<?OFX OFXHEADER=\"200\" SECURITY=\"NONE\" OLDFILEUID=\"NONE\" VERSION=\"200\" NEWFILEUID=\"NONE\"?>";
		ofx += this._br;
		ofx += this._br;
		ofx += "<OFX>";
		ofx += this._br;
		ofx += this.tab(1) + "<SIGNONMSGSRSV1>";
		ofx += this._br;
		ofx += this.tab(2) + "<SONRS>";
		ofx += this._br;
		ofx += this.tab(3) + "<STATUS>";
		ofx += this._br;
		ofx += this.tab(4) + "<CODE>0</CODE>";
		ofx += this._br;
		ofx += this.tab(4) + "<SEVERITY>INFO</SEVERITY>";
		ofx += this._br;
		ofx += this.tab(3) + "</STATUS>";
		ofx += this._br;
		ofx += this.tab(3) + "<DTSERVER>" + dataDate + "</DTSERVER>";
		ofx += this._br;
		ofx += this.tab(3) + "<LANGUAGE>ENG</LANGUAGE>";
		ofx += this._br;
		ofx += this.tab(2) + "</SONRS>";
		ofx += this._br;
		ofx += this.tab(1) + "</SIGNONMSGSRSV1>";
		ofx += this._br;
		ofx += this.tab(1) + "<BANKMSGSRSV1>";
		ofx += this._br;
		ofx += this.tab(2) + "<STMTTRNRS>";
		ofx += this._br;
		ofx += this.tab(3) + "<TRNUID>1</TRNUID>";
		ofx += this._br;
		ofx += this.tab(3) + "<STATUS>";
		ofx += this._br;
		ofx += this.tab(4) + "<CODE>0</CODE>";
		ofx += this._br;
		ofx += this.tab(4) + "<SEVERITY>INFO</SEVERITY>";
		ofx += this._br;
		ofx += this.tab(3) + "</STATUS>";
		ofx += this._br;
		ofx += this.tab(3) + "<STMTRS>";
		ofx += this._br;
		ofx += this.tab(4) + "<CURDEF>" + this._currency + "</CURDEF>";
		ofx += this._br;
		ofx += this.tab(4) + "<BANKACCTFROM>";
		ofx += this._br;
		ofx += this.tab(5) + "<BANKID>" + bankName + "</BANKID>";
		ofx += this._br;
		ofx += this.tab(5) + "<ACCTID>" + accountNumber + "</ACCTID>";
		ofx += this._br;
		ofx += this.tab(5) + "<ACCTTYPE>" + this._accttype + "</ACCTTYPE>";
		ofx += this._br;
		ofx += this.tab(4) + "</BANKACCTFROM>";
		ofx += this._br;
		
		return ofx;
	}

	getOFX2Transactions(transactions, dataDate)
	{

		// iterate through the array
		let ofx = this.tab(4) + "<BANKTRANLIST>";
		ofx += this._br;
		
		// <DTSTART><DTEND> - can represent the time/date when the server started/stopped *looking* for the data, not the first/last transction.
		ofx += this.tab(5) + "<DTSTART>" + dataDate + "</DTSTART>";
		ofx += this._br;
		ofx += this.tab(5) + "<DTEND>" + dataDate + "</DTEND>";
		ofx += this._br;
		
		// optimized loop
		for (var i = transactions.length - 1; i > 0; i--) {

			let trntype = (transactions[i].debit) ? "DEBIT" : "CREDIT";
			// date in ymdHis format
			let date = this._form_ymdHis(new Date(transactions[i].date + " 00:00:00"));

			// add minus for debit
			let trnamt = (transactions[i].debit) ? "-" + transactions[i].debit : transactions[i].credit;
			let uniqueFitId = this._generateFitId(date, i);

			let transactionName = "";
			if(transactions[i].lib2) {
				transactionName = transactions[i].remittanceInformation + " / " + transactions[i].lib2;
			} else {
				transactionName = transactions[i].remittanceInformation;
			}

			ofx += this.tab(5) + "<STMTTRN>";
			ofx += this._br;
			ofx += this.tab(6) + "<TRNTYPE>" + trntype + "</TRNTYPE>";
			ofx += this._br;
			ofx += this.tab(6) + "<DTPOSTED>" + date + "</DTPOSTED>";
			ofx += this._br;
			ofx += this.tab(6) + "<TRNAMT>" + trnamt + "</TRNAMT>";
			ofx += this._br;
			ofx += this.tab(6) + "<FITID>" + uniqueFitId + "</FITID>";
			ofx += this._br;
			ofx += this.tab(6) + "<NAME>" + transactionName.trim() + "</NAME>";
			ofx += this._br;
			ofx += this.tab(5) + "</STMTTRN>";
			ofx += this._br;
		} // end for loop
		
		ofx += this.tab(4) + "</BANKTRANLIST>";
		ofx += this._br;

		return ofx;
	}


	getOFX2Footer(balance, dataDate)
	{	
		// prepare the OFX footer
		let ofx = this.tab(4) + "<LEDGERBAL>";
		ofx += this._br;
		
		ofx += this.tab(5) + "<BALAMT>" + balance + "</BALAMT>";
		ofx += this._br;
		ofx += this.tab(5) + "<DTASOF>" + dataDate + "</DTASOF>";
		ofx += this._br;
		ofx += this.tab(4) + "</LEDGERBAL>";
		ofx += this._br;
		ofx += this.tab(3) + "</STMTRS>";
		ofx += this._br;
		ofx += this.tab(2) + "</STMTTRNRS>";
		ofx += this._br;
		ofx += this.tab(1) + "</BANKMSGSRSV1>";
		ofx += this._br;
		ofx += "</OFX>";
		ofx += this._br;
		
		return ofx;
	}

    // add n tabs
    tab(n)
    {
    	let output = "";
    	
    	for (var i = 0; i < n; i++)
		{
			output += this._tab;
		}

		return output;
    }

	_generateFitId(dateTransaction, index)
	{
		//md5 de la date de la transaction + index pour éviter des hash identiques, return en MAJ
		var md5 = calcMD5(dateTransaction + index);

		return md5.toUpperCase();
	}

	_form_ymdHis(xDate) {
		return xDate.getFullYear().toString(10)
			+ (xDate.getMonth()+1).toString(10).padStart(2,'0')
			+ xDate.getDate().toString(10).padStart(2,'0')
			+ xDate.getHours().toString(10).padStart(2,'0')
			+ xDate.getMinutes().toString(10).padStart(2,'0')  
			+ xDate.getSeconds().toString(10).padStart(2,'0')
	}

  	_downloadFile(output, fileName) {
	    const link = document.createElement('a');
	    document.body.appendChild(link);
	    link.download = fileName;
	    link.href = output;
	    link.click();
	}

}