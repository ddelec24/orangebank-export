
var DEBUG_MODE = false;
// injecteur pour récupérer données et infos compte
function interceptXHR() {
  var xhrOverrideScript = document.createElement('script');
  xhrOverrideScript.type = 'text/javascript';
  xhrOverrideScript.innerHTML = `
  (function() {

	var XHR = XMLHttpRequest.prototype;
	var send = XHR.send;
	var open = XHR.open;
	XHR.open = function(method, url, async) {
		this.url = url; // the request url
		return open.apply(this, arguments);
	}
	XHR.send = function() {
		this.addEventListener('readystatechange', function() { // @TODO TESTER EN loadend et virer la vérif de readyState
			if (this.url.includes('operations/all')) {
				if(this.readyState == 4) {
					var dataDOMElement = document.createElement('div');
					dataDOMElement.id = '__interceptedData';
					dataDOMElement.innerText = this.response;
					dataDOMElement.style.height = 0;
					dataDOMElement.style.overflow = 'hidden';
					document.body.appendChild(dataDOMElement);
				}
			}    
			if (this.url.includes('equipments')) {
				if(this.readyState == 4) {
					var dataDOMElement = document.createElement('div');
					dataDOMElement.id = '__interceptedAccount';
					dataDOMElement.innerText = this.response;
					dataDOMElement.style.height = 0;
					dataDOMElement.style.overflow = 'hidden';
					document.body.appendChild(dataDOMElement);
				}
			}       
		});
		return send.apply(this, arguments);
	};
  })();
  `
  document.head.prepend(xhrOverrideScript);
}

// en attente que le document soit chargé
function checkForDOM() {
  if (document.body && document.head) {
	interceptXHR();
  } else {
	requestIdleCallback(checkForDOM);
  }
}
requestIdleCallback(checkForDOM);

// récupération des données de compte et d'opérations
function scrapeData() {
	
	var currentDate = new Date().toISOString().slice(0,10); // date du jour, format YYYY-MM-DD

	//var reg = new RegExp("^([0-9]{2}/[0-9]{2} - )(.*)$", "i"); 
	// regex pour capturer date dans libellé (option future possible pour retirer la date dans le libellé)

	// infos du compte
	var responseContainingEle = document.getElementById('__interceptedAccount');
	if (responseContainingEle) { // ajouter sizeof  responseContainingEle.innerHTML ?
		var response = JSON.parse(responseContainingEle.innerHTML);
		//console.log(response);
		var accountNumber = response["mainEquipment"]["number"];
		var bankName = "Orange Bank";
		var balance = response["mainEquipment"]["availableBalance"];
	}

	// infos sur les opérations
	var responseContainingEle = document.getElementById('__interceptedData');
	if (responseContainingEle) { // ajouter sizeof  responseContainingEle.innerHTML ?

		var response = JSON.parse(responseContainingEle.innerHTML);
		//console.log("Preparing CSV file...");
		var csvFile = [];
		// premiere ligne avec la balance
		csvFile.push({
			"operationDate": "",
			"date": "",
			"debit": "",
			"credit": "",
			"transactionCurrency": "",
			"remittanceInformation": "",
			"lib2": "",
			"type": "",
			"transferNumber": "",
			"balance": balance
		});

		var countMonthOperations = 0;
		var d = new Date();
		//getmonth va de 0 à 11 faut ajouter 1 et rajoute un 0 devant si besoin
		var currentMonth = (parseInt(d.getMonth()) + 1).toString().padStart(2, '0');
		var currentYear = (parseInt(d.getFullYear())).toString();
		
		// création du tableau des trasations
		$.each(response.accountOperations, function( indexDate, valueDate ) {
		  $.each(valueDate, function( indexOperation, operation ) {

			let debit = null;
			let credit = null;
			let operationDate = operation.operationDate;
			let date = operation.date;
			let dateMonthYear = date.split("-"); // YYYY-MM-DD

			let creditDebitIndicator = operation.creditDebitIndicator; // DBIT ou CRDT
			if(creditDebitIndicator == "DBIT") {
				debit = Math.abs(operation.transactionAmount); // valeur absolue
			} else {
				credit = operation.transactionAmount;
			}

			let transactionCurrency = operation.transactionCurrency; // EUR etc
			let remittanceInformation = operation.remittanceInformation;

			// repositionne la date en fin du libellé si besoin
			//let matches = remittanceInformation.match(reg);
			//let lib1 = matches ? matches[2] + " - " + matches[1].substring(0, 5) : remittanceInformation; // info banque transaction

			let lib2 = operation.lib2; // libellé personnel

			let type = operation.type; // VIRREC / PAICBP
			let transferNumber = operation.transferNumber; // code de transfert interne

			// pour ne prendre que les achats CB et dans le mois courant
			if((currentMonth == dateMonthYear[1]) && (currentYear == dateMonthYear[0]) && (type == "PAICBP")) { 
				countMonthOperations++;
			}

			csvFile.push({
							"operationDate": operationDate,
							"date": date,
							"debit": debit,
							"credit": credit,
							"transactionCurrency": transactionCurrency,
							"remittanceInformation": remittanceInformation,
							"lib2": lib2,
							"type": type,
							"transferNumber": transferNumber,
							"balance": ""
						});
		  });

		});

		// initialisation du tableur
		const xls = new xlsExport(csvFile, accountNumber + ' ' + currentDate);

		// initialisation du converter ofx
		const ofx = new ofxConverter();

		// îcones encodées en base 64
		let b64CSV = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAADSklEQVRYhd2ZsWvbWhSHP3kOGN4QCs0Y5CEPAu2U1VryoH+Ana6BEuQ9Q9CS3d4LDnhp/PCawrt7MBlKTe28sd4KqTwUHJyh0+8NslRLkRT7NbWd/sDLveic755z7pXusSWJ/6EisAe8BP4EtoEt4I/p/DfgC/AZ+Bf4CFwB40UdWQsC7gGvgH3gxYK+eoAB3hPAzqV5AV8Cr4Eq8GxBsKS+Am3gHUFk8yUp72dJOpL0SY+vT1PbVh5DHtxzSY1fAJZUY+prIcBtSa0lwIVqTX3OBfh8yXCzkPcimVZzy0hrlhpK1GQS8Gh1bJGONMNUSBwlb37yCHkMvSFgAYgBvgZ2l45zX7sELMAPwD2CQ3hdVCVgigBf8fNviMfUMwImCgQv/v2V4qRrHygWCEKZ++IfjUY0Gg0cx8GyLEqlEo7jMBgMABgMBlSrVSzLwrIsHMdhNBoBUCqVqNVqqXYdx8FxnCy3L4A9JJ3k7flutytAtm2r2WzKGKNOpyPP8zSZTDQcDgXIdV0ZY2SMked50fOe5wnQZDKJ2Q2f63Q6ee5PkNTOmg2NzDpMql6vpwIkF2iMiY03m00B8n0/D7BdIPjYTFWr1QLg+Pg4M/3FYhEA3/dT53d3g5Or2+3Gxs/Pz6lUKmxubmbaBraRdJOFXy6X5bpu3grl+75s25Zt2+p0OqmRTKa53+/Pk15JukHS96xZQPV6/SEj8n1frutGtZpMZ5jmbrcr6Ud6s8piRt9zAW3bzq2/PNB+vx+NTyaTWC3Pk5lZwMwUu64r27bnWek9mGTkQ1thesNoPqAbJH3Img2NLRLFELDZbMbGjTHRcbTAoj9YktpAJWsbnZ2dcXh4SLlc5uDggK2tLW5vb7m+vqZWq3FxccF4PGZnZyfa+b1ej8vLy9gOvbu7Y2NjAwDP8zg9Pc3bvaH+fvCgDiMZrhyIasj3fRljVKlUBEQRGg6HqXbS6vMBnViS9oF/5lnOCvRXgeAS3Vs1SYp6wFWBoB1hVgyTJgOMw+/B9wQ3/nXRVwKm6IP1iqAdsS5qM+3fzN5J3gH9leDE1SdgAeKAH4G3S8e5r7fMNpX0xC7ua9/6eBLNoyfRflubBubat4B/myZ6Ukv7G+I/oeIwhQo6NbkAAAAASUVORK5CYII=';
		let b64OFX = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAADIUlEQVRYhd2ZsWrbWhjHf8fcLYOhQyi0UyhnKhTaKRk15UIfQKYP0AQnT5AobxDtBQ9dioPnQvUCdobQUIc7hdrThVYZLgko8/8OshRJlhS7zbWd+wMN1nek85N0zqejz0YSv0AT2ATeAC+BF8Bz4Mkk/g/wN/Ad+Av4CpwCN/N2ZOYU3ATeAtvA6zn7OgcC4DOx7EzMKvgGeAe0gKdzihX5CXSBT8R3th5JdZuRtCvpmx6eb5NzmzqHOrlnkvz/QKyIP+lrLsEXkj4uQC7h46TPmQSfLVguKzl1J8vG3CIeaxW+CmOyKLi7PLeUXWWcGoVU8v43U8hD8J7YBSAn+A54tXCdaV4RuwB3gpvESXhVaBE7pYJv+f03xEPylNiJBvGLf3upOuVsA80G8a2sffEPBgNarRbGGIwx7O3tMR6Pc21830/j2W0wGHBxcYExBt/3c8eMx2OMMRwdHZV1+xrYRNJB3ZzvdDoC5HmegiBQEARyHEeAhsNh2u74+FhA2ibZwjCUJHmeJ0Cj0Sg9pt1uy1qbtinhAEndquhwOBSgXq+X2x9FkVzXlbV2SrCKMAxlrVW73ZYk9fv99IJq6CLprCrqeV5OIkvSQb/fn0lQknq9XnqM4zhyXbe2vaQzJP2oimavuEgURQLU6XRmFpSUDo/i467gR4O7ZfoUl5eXbGxslMbW1tYAuLnJr+KLk+Tq6ioX39nZAcB13cpzZ3jyR13UWsv19XVp7Pb2FoBms5nbHwRB7vf6+nrumMPDQ6y1nJycsL+/z9bWVr1i3SNOZlkZvzIGk4wQhqEcx5HjOIqiqPYR106SRKJsFicdJNwnOBqNcmM2yRDJ7wrOatOMdJe/innQWluaB6tI0lL2jiXnrsmD3XsTtaRccrbWyvO8qRlYJxgEQWnOK+bGEg6MpG3gS/1IXRp/Nog/os+XbVLCOXDaIC5HBPc0XgYBcJOsBz8Tf/GvCj+JndIF6ylxOWJV6DKp32S/ST4Bw6Xo5BkSuwB5wa/Ah4XrTPOBbFFJj+zDfeVLH4+iePQoym8rU8Bc+RLw/6aIXmRhf0P8C5ODBq5UL5yGAAAAAElFTkSuQmCC';
		//ajout bouton xlsExport, rajoute bouton dans le site en manipulant le DOM
		$('.ob-toggle-rib-btn').parent().prepend('<button class="csv-btn mr-m"><span class="sr">Export CSV</span>' +
			'<img src="' + b64CSV + '" />' +
			'</button>');
		//ajout bouton generateOFX, rajoute bouton dans le site en manipulant le DOM
		$('.ob-toggle-rib-btn').parent().prepend('<button class="ofx-btn mr-m"><span class="sr">Export OFX</span>' +
			'<img src="' + b64OFX + '" />' +
			'</button>');

		//ajout du nombre d'opérations pour le mois courant.
		$('.ob-toggle-filter-btn').parent().prepend('<div style="vertical-align: middle; line-height: 40px; height: 40px; margin-right: 20px">' +
			'<strong>' + countMonthOperations + '</strong> opérations ce mois-ci.' +
			'</div>');

		// pour capter l'action du bouton d'export CSV, lance le téléchargement
		$('.csv-btn').on('click', function() {
			xls.exportToCSV(accountNumber + ' ' + currentDate + '.csv');
		});
		// pour capter l'action du bouton d'export OFX, lance le téléchargement
		$('.ofx-btn').on('click', function() {
			ofx.generateOFX(bankName, accountNumber, balance, csvFile, currentDate);
		});
		
	} else {
		requestIdleCallback(scrapeData);
	}
}
requestIdleCallback(scrapeData);
