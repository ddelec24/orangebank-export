Orange Bank ne possède pas d'export CSV ou OFX pour gérer son budget sur  
une appli de gestion de budget comme Homebank ou autre.

Cette extension ajoute 2 boutons dans la liste des opérations  
pour récupérer les fichiers déjà prêts à être utilisées

![](img/capture.png)

### **TODO**

- [ ] vérifier qu'on récupère toutes les opérations y compris les non affichées  
sur la page orange bank.
- [ ] Mettre en place la récupération lorsque l'on utilise les filtres
- [ ] Tester l'injecteur avec le loadend et virer la vérif de readyState
- [ ] Ajouter dans le menu popup le nombre d'opérations réalisée dans le mois
- [ ] Appliquer l'injecteur lorsqu'un filtre est appliqué 
- [ ] Ne pas prendre en compte les opérations en attente ou empreintes

### **TO FIX**

- [x]  encodage notamment du ° pour les numéros de cartes (changer le charset  
dans le MIME à la génération ne résoud pas)
- [x] créer des vraies îcones pour csv et ofx
- [ ] Quand on va dans une autre section et qu'on revient, les boutons  
ne sont plus présents


### **INSTALLATION**

Sous **chrome**, il suffit d'aller dans les *"..."* verticaux, *"plus d'outils"*  
puis *"extensions"*.  
Ensuite dans l'onglet qui s'est ouvert, *"Chargez l'extension non empaquetée"*  
et sélectionner le répertoire qui contient la source. 

Sous **Firefox**, il faut ouvrir un onglet et écrire *"about:debugging"*  
puis *"charger un module temporaire"*.  
Prendre alors n'importe quel fichier dans le répertoire qui contient la source.  
> /!\ Attention sous firefox il faut refaire la manip à lancement firefox...


Je ne soumets pour le moment pas les extensions sur les stores de chrome et FF,  
j'espère bien qu'Orange Bank va intégrer nativement ces fonctionnalités!  
J'ai juste fait ça pour moi car je ne veux pas perdre des transactions.  

Testé avec Homebank sous linux mais le fichier OFX respecte le standard  
concernant l'activité de compte classique, donc ça doit fonctionner avec  
tous les logiciels de suivi budget existants.